# coding: utf-8
from Orange.data import Table, Domain,DiscreteVariable, ContinuousVariable

import numpy as np
from sklearn import   datasets
digits = datasets.load_digits(n_class=6)
X = digits.data
y = digits.target
n_features = X.shape[1]
def make_Xy(X,y):
    xmax = X.ravel().max()
    X2 = X.copy()
    X2 /= xmax
    Xnew = np.hstack( (X2,y[:,np.newaxis]) )
    label = []
    vartype = []
    classtype = []
    for i in range(X.shape[1]):
        label.append(str(i))
        vartype.append("continuous")
        classtype.append("")
    label.append("y")
    vartype.append("discrete")
    classtype.append("class")
    return Xnew,label,vartype,classtype
Xnew,label,vartype,classtype = make_Xy(X,y)
def write_file(filename,X,label,vartype,classtype):
    f = open(filename,"wt",encoding="utf-8")
    f.write(",".join(label)+"\n")
    f.write(",".join(vartype)+"\n")
    f.write(",".join(classtype)+"\n")
    for i,x in enumerate(X):
        s = list(map(str,x))
        #print(i)
        f.write(",".join(s)+"\n")
    f.close()
write_file("digits100.csv",Xnew[:100,:],label,vartype,classtype)
#domain = Domain.from_numpy(Xnew)


def make_table(Xnew,n_features):
  label = []
  for i in range(n_features):
    label.append(ContinuousVariable.make(str(i)))
  label.append(DiscreteVariable.make("y"))
  newdomain = Domain(label)
  return   Table(newdomain,Xnew[:100,:])
out_data = make_table(Xnew,n_features)

print("all done")